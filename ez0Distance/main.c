/*
 * ez0Distance.c
 *
 * Created: 16/08/2018 10:53:04
 * Author : jzrhard06
 */ 

/*
 C�digo Fonte do Rob� US-1
 
 MCU: Atmega328P
 
 Autor: Eng. Wagner Rambo
 
 Data: Outubro 2016
 */ 

// --- Frequ�ncia do Oscilador Externo ---
#define F_CPU 16000000									//Configurado para 16Mz


// --- Bibliotecas Auxiliares ---
#include <avr/io.h>										//biblioteca de entradas e sa�das padr�o
#include <avr/interrupt.h>								//biblioteca para interrup��es
#include <util/delay.h>									//biblioteca com as fun��es de delay
#include <string.h>

// --- Prot�tipo das Fun��es Auxiliares ---
void HCSR04Trig();										//Fun��o para gerar o pulso de trigger para o sensor ultrass�nico


// --- Mapeamento de Hardware ---
#define     echo		PORTD2		//bit para leitura do pulso de Echo
#define		trigger		PORTD3		//bit para gerar trigger para o sensor ultrass�nico
#define		sentido1	PORTB5		//bit de controle de sentido do motor1
#define		pwm1		PORTD5		//bit de ajuste de pwm do motor1
#define		sentido2	PORTD6		//bit de controle de sentido do motor2
#define		pwm2		PORTD7		//bit de ajuste de pwm do motor2
#define		sensor_esq	PORTB0		//sensor da esquerda (bumper de colis�o)
#define		sensor_dir	PORTB1		//sensor da direita (bumper de colis�o)


// --- Mnem�nicos ---
#define   set_bit(reg,bit)		(reg |= (1<<bit))		//macro para setar um bit de determinado registrador
#define   clr_bit(reg,bit)		(reg &= ~(1<<bit))		//macro para limpar um bit de determinado registrador

uint16_t pegaPulsoEcho();						//fun��o para ler pulso de Echo
uint16_t pegaDist();
void itoa(int i, char* s);
char itoc(int i);

// --- Vari�veis Globais ---
short counter = 0x00;

// --- Interrup��es ---
ISR(TIMER0_OVF_vect)
{
	counter++;                      //Incrementa counter
	
	if(counter == 0x0F)             //counter igual a 15?
	{                               //Sim...
		
	 	counter = 0x00;				//zera counter
		 
		HCSR04Trig();               //pulso de trigger
		
	} //end if counter
	
} //end ISR Timer0


void init(){
	//Para Fosc = 16M, U2Xn = 0 e um baud rate = 9600, temos que UBRR0 = 103d = 00000000 01100111
	//Para um baud rate = 115200, temos UBRR0 = 8d = 00000000 00001000
	UBRR0H = 0b00000000;//9600 bits/s.
	UBRR0L = 0b01100111;

	UCSR0A = 0b01000000; //TXC0 = 1 (zera esse flag), U2X0 = 0 (velocidade normal),
	//MPCM0 = 0 (desabilita modo multiprocessador).
	UCSR0B = 0b00011000; //Desabilita interrup��o recep��o (RXCIE0 = 0), transmiss�o (TXCIE0 = 0)
	//e buffer vazio (UDRIE0=0), habilita receptor USART RXEN0 = 1
	//(RxD torna-se uma entrada), habilita transmissor TXEN0 = 1
	//(TxD torna-se uma sa�da), seleciona 8 bits de dados (UCSZ2 = 0).
	UCSR0C = 0b00000110; //Habilita o modo ass�ncrono (UMSEL01/00 = 00), desabilita paridade (UPM01/00 = 00),
	//Seleciona um bit de stop (USBS0 = 0), seleciona 8 bits de dados (UCSZ1/0 = 11) e
	//sem polaridade (UCPOL0 = 0 - modo ass�ncrono).
}


void send(char *message){
	int i;
	while((UCSR0A & 0x20) != 0x20){}//Verifica se UDR0 pode receber novos dados para Tx.
	
	for(i=0; i < (strlen(message) + 1); i++){
		if(i == (strlen(message)) - 1 ){
			UDR0 = ',';
			while (!(UCSR0A & (1 << UDRE0)));
		}
		
		UDR0 = message[i];
		while (!(UCSR0A & (1 << UDRE0)));
	}
	
	UDR0 = '\r';
	while (!(UCSR0A & (1 << UDRE0)));
	UDR0 = '\n';
	while (!(UCSR0A & (1 << UDRE0)));
	
	UCSR0A = 0x40;//Para zerar TXC0 (bit 6).
}

// --- Fun��o Principal ---
int main(void)
{
	int pulso;
	char distance[10];
	init();
	 
	set_bit(DDRD,trigger);							    //configura trigger como sa�da
	clr_bit(DDRB,PORTB0);								//configura PB0 como entrada
	clr_bit(DDRB,PORTB1);								//configura PB1 como entrada
	
	set_bit(PORTB,sensor_dir);                          //habilita pull-up interno
	set_bit(PORTB,sensor_esq);                          //habilita pull-up interno
	
	PORTD = 0x00;										//inicia PORTD em low
	
	
	cli();								//Desabilita a interrup��o global
   	   	
   	TCNT0 =  0x00;						//Inicia o timer0 em 0
   	TCCR0B = 0x04;						//Configura o prescaler para 1:256
   	TIMSK0 = 0x01;						//Habilita a interrup��o por estouro do TMR0
   	
   	sei();								//Habilitar a interrup��o global
	// --
	
		
	
    while(1)											//Loop infinito
    {
		pulso = pegaDist();
	
		if(pulso < 20) set_bit(PORTB,sentido1);
		else clr_bit(PORTB,sentido1);
		
		itoa(pulso, distance);
		send(distance);
	   _delay_ms(1000);
    } //end while
	
	
} //end main



// --- Desenvolvimento das Fun��es ---

char itoc(int i) {
	switch (i) {
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';
		case 4: return '4';
		case 5: return '5';
		case 6: return '6';
		case 7: return '7';
		case 8: return '8';
		case 9: return '9';
	}
}

void itoa(int i, char* s) {
	int m10 = 1;
	int qt = 0;
	int n;
	
	while (((int)(i/m10))>0)
	m10 *= 10;

	m10 /= 10;
	
	while (m10>0) {
		n = (int)(i/m10) - ((int)(i/(m10*10))*10);
		s[qt] = itoc(n);
		m10 /= 10;
		qt++;
	}
	s[qt] = '\0';
}


uint16_t pegaDist()
{
	return pegaPulsoEcho()/5.8;
}

uint16_t pegaPulsoEcho()								//Fun��o para captura do pulso de echo gerado pelo sensor de ultrassom
{
	uint32_t resultado;								//Vari�veis locais auxiliares
	
	while(!(PIND & (1<<echo)));
	
//	for(i=0;i<600000;i++)							//La�o for para aguardar o in�cio da borda de subida do pulso de echo
//	{
//		if(!(PIND & (1<<echo)))							//Pulso continue em n�vel baixo?
//		continue;										//Aguarda
//		else                                            //Pulso em n�vel alto?
//		break;											//Interrompe
//	} //end for
	
	
	//Configura��o do Timer1 para contar o tempo em que o pulso de echo permanecer� em n�vel l�gico alto
	
	TCCR1A = 0x00;										//Desabilita modos de compara��o A e B, Desabilita PWM
	TCCR1B = (1<<CS11);									//Configura Prescaler (F_CPU/8)
	TCNT1  = 0x00;										//Inicia contagem em 0
	
	while((PIND & (1<<echo)) && !(TCNT1 > 60000));
	
//	for(i=0;i<600000;i++)								//La�o for para aguardar que ocorra a borda de descida do pulso de echo
//	{
//		if(PIND & (1<<echo))							//Pulso continua em n�vel alto?
//		{
//			if(TCNT1 > 60000) break;					//Interrompe se TCNT1 atingir o limite da contagem
//			else continue;								//Sen�o, continua
//		}
//		else
//		break;											//Interrompe quando encontrar a borda de descida
		
//	} //end for
	
	resultado = TCNT1;									//Salva o valor atual de TCNT1 na vari�vel resultado (tempo que o echo ficou em high)
	
	TCCR1B = 0x00;										//Interrompe Timer
	
	
	return (resultado>>1);								//Fun��o retornar� o tempo em microssegundos
	
	
} //end pegaPulsoEcho


void HCSR04Trig()								//gera pulso de Trigger
{
	
	set_bit(PORTD,trigger);
	_delay_us(10);
	clr_bit(PORTD,trigger);
	
} //end HCSR04Trig




